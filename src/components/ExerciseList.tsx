import React from 'react';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

import MusicNoteIcon from '@material-ui/icons/MusicNote';

import Exercise from '../MidiPlayer/Exercise';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toolbar: theme.mixins.toolbar,
  }),
);

interface ExerciseListProps {
  exercises: Exercise[];
  exerciseOnClick: (exercise: Exercise) => void;
}

const ExerciseList: React.FC<ExerciseListProps> = (props: ExerciseListProps) => {
  const classes = useStyles({});

  return (
    <div>
      <div className={classes.toolbar} />
      <Divider />
      <List>
        <ListItem>
          <ListItemText primary="Exercises" />
        </ListItem>
        {props.exercises.map((exercise: Exercise) => (
          <ListItem button key={exercise.name} onClick={() => props.exerciseOnClick(exercise)}>
            <ListItemIcon><MusicNoteIcon /></ListItemIcon>
            <ListItemText primary={exercise.name} />
          </ListItem>
        ))}
      </List>
    </div>
  );
};

export default ExerciseList;