import MidiPlayer from 'midi-player-js';
import Soundfont from 'soundfont-player';

/**
 * Class for playing sound in the browser given a MIDI file.
 * Has control for play, pause, etc.
 */
export default class MidiSoundPlayer {
  private _instrument?: Soundfont.Player;
  private _player?: MidiPlayer.Player;
  private _nodeMap = new Map<string, Soundfont.Player>();
  private _ac: AudioContext;

  constructor() {
    // retrieve the instrument from Soundfont
    this._ac = new window.AudioContext();

    let setInstrument = (instrument: Soundfont.Player) => {
      this._instrument = instrument;
    };

    let boundSetInstrument = setInstrument.bind(this);
    Soundfont.instrument(this._ac, 'acoustic_grand_piano').then(boundSetInstrument);
  }

  /**
   * Loads a specific midi file and prep for playing
   * @param filename the name of the file to load
   * @param onLoadCallback function to execute after file load completes
   * @param onEndCallback function to execute when the file finishes playback
   */
  public loadFile(filename: string, { onLoadCallback = () => {}, onEndCallback = () => {} }) {
    // Binding the current MidiSoundPlayer via 'this' so the class members
    // can be accessed within the callback stack.
    let playMidiEvent = this.playMidiEvent.bind(this);
    this._player = new MidiPlayer.Player(playMidiEvent);
    this._player!.on('fileLoaded', onLoadCallback);
    this._player!.on('endOfFile', onEndCallback);

    this.getMidiFile(filename, this._player!);
  }

  /**
   * Plays audio from the currently loaded MIDI file
   */
  public play() {
    if (!this._player) {
      return;
    }
    this._player!.play();
  }

  /**
   * Pauses audio for the currently loaded MIDI file
   */
  public pause() {
    if (!this._player) {
      return;
    }
    this._player!.pause();
    this._instrument!.stop();
  }

  /**
   * Returns the total running time of the current MIDI file 
   * in number of seconds
   */
  public getTotalTime(): number {
    if (!this._player) {
      return NaN;
    }
    return this._player!.getSongTime();
  }

  /**
   * Returns the current time in the MIDI file in seconds
   */
  public getCurrentTime(): number {
    if (!this._player) {
      return NaN;
    }
    let total = this._player!.getSongTime();
    let remaining = this._player!.getSongTimeRemaining();
    let currentSeconds = total - remaining;
    return currentSeconds;
  }

  /**
   * Method passed to MidiPlayer constructor for handling MIDI events 
   * as they're emitted by the player.
   * @param instrument the Soundfont player to play sounds with
   * @param event the current MIDI event emitted by MidiPlayer
   */
  private playMidiEvent(event: MidiPlayer.Event) {
    if (!this._instrument) {
      console.error('Attempting to play sound without initiating soundfont player');
      return;
    }
    if (event.name === 'Note on' && event.velocity !== 0) {
      if (!event.noteName || !event.channel) {
        console.warn('Received Note on event with no channel or noteName: ' + event);
        return;
      }
      let node = this._instrument!.play(event.noteName!, this._ac.currentTime, { gain: event.velocity!/100 });
      this._nodeMap.set(event.channel + event.noteName!, node);
    }

    // Sometimes note off events can come in the form of note on with 0 velocity
    if ((event.name === 'Note on' && event.velocity === 0) || event.name === 'Note off') {
      if (!event.noteName || !event.channel) {
        console.warn('Received Note off event with no channel or noteName: ' + event);
        return;
      }
      let node = this._nodeMap.get(event.channel + event.noteName!);
      if (!node) {
        console.warn('Attempting to stop a note that is not playing. ' + event);
        return;
      }
      node.stop();
    }
  }
  
  /**
   * Sends a GET request to grab the midi file from the server,
   * and loads it into the MidiPlayer.
   * @param midiFilename the filename of the MIDI file to retrieve
   * @param player the MidiPlayer to load the midi file into
   */
  private getMidiFile(midiFilename: string, player: MidiPlayer.Player) {
    let oReq = new XMLHttpRequest();
    oReq.open('GET', '/midi/' + midiFilename, true);
    oReq.responseType = 'arraybuffer';

    oReq.onload = function (oEvent) {
      let arrayBuffer = oReq.response;
      player.loadArrayBuffer(arrayBuffer);
    };

    oReq.send(null);
  }
}
