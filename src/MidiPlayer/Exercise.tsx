/**
 * Object representing an exercise
 */
export default class Exercise {
  constructor(
    public name: string, 
    public filename: string) {
      
  }

  public toJSON(): IJSONExercise {
    return Object.assign({}, this, {});
  }

  static fromJSON(json: IJSONExercise|string): Exercise {
    if (typeof json === 'string') {
      return JSON.parse(json, Exercise.reviver);
    } else {
      let exercise = Object.create(Exercise.prototype);
      return Object.assign(exercise, json, {});
    }
  }

  static reviver(key: string, value: any): any {
    return key === '' ? Exercise.fromJSON(value) : value;
  }

  static getExerciseJSON(): Promise<IJSONExercise[]> {
    return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      xhr.open('GET', '/midi/exerciseList.json');
      xhr.responseType = 'json';
      xhr.onload = () => { resolve(xhr.response); };
      xhr.onerror = () => { reject(xhr.statusText); };
      xhr.send();
    });
  }
}

export interface IJSONExercise {
  name: string;
  filename: string;
}